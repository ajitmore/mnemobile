import { Component } from "@angular/core";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
import { LoadingController, ToastController } from "@ionic/angular";

import { timer } from "rxjs";

import { seeds, seedings } from "./data";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  private product: FormGroup;
  today: String = new Date().toISOString();

  constructor(
    private barcodeScanner: BarcodeScanner,
    private formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public toastController: ToastController
  ) {
    this.product = this.formBuilder.group({
      seedClass: [{ value: "", disabled: true }],
      batchNumber: [{ value: "", disabled: true }],
      seedLot: [{ value: "", disabled: true }],
      type: [{ value: "", disabled: true }],
      // nurserySeedlingId: [{ value: "", disabled: true }],
      location: "",
      seller: "",
      quantity: "",
      totalCost: "",
      transactionDate: ""
    });
  }

  onSave() {
    console.log(this.product.value);
    this.showLoader().then(res => {
      timer(1250).subscribe(() => {
        this.showToast();
        this.product.reset();
      });
    });
  }

  onScan() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        /*console.log(barcodeData);
         alert(
          `Text: ${barcodeData.text} Format: ${barcodeData.format} Cancelled:${
            barcodeData.cancelled
          }`
        ); */

        this.setBarcodeData(barcodeData.text);
      })
      .catch(err => {
        alert(err);
        // this.setBarcodeData("00002");
      });
  }

  setBarcodeData(data) {
    const seedId = data;
    const seed = seeds.find(seed => seed.seedLot === seedId);
    if (seed) {
      this.product.patchValue({
        seedClass: seed.seedClass,
        batchNumber: seed.batchNumber,
        seedLot: seed.seedLot,
        type: seedings[0].type, //hard coded as it contains one element in array
        nurserySeedlingId: seedings[0].nurserySeedlingId
      });
    }
  }

  async showLoader() {
    const loading = await this.loadingController.create({
      duration: 1000,
      message: "Please wait...",
      translucent: true
    });
    return await loading.present();
  }

  async showToast() {
    const toast = await this.toastController.create({
      message: "Record saved",
      showCloseButton: true,
      position: "bottom",
      duration: 1000,
      closeButtonText: "Close"
    });
    toast.present();
  }
}
