export const seeds = [
  {
    seedLot: "00001",
    species: "genus Allium",
    seedClass: "White Onion",
    seedZone: "Gujrat",
    basicMaterialType: "kharif & hot weather",
    regionOfProvenence: "Gujrat",
    origin: "MP",
    registerRefNoOfSeedCertIssuedByNDA: "",
    batchNumber: "ONION1",
    purity: "100",
    geneticWorth: "",
    germinationRate: 90,
    seedPerPacket: 400,
    packetsAvailable: 100,
    cost: 300
  },
  {
    seedLot: "00002",
    species: "genus Allium",
    seedClass: "Red Onion",
    seedZone: "Gujrat",
    basicMaterialType: "kharif & hot weather",
    regionOfProvenence: "Gujrat",
    origin: "MP",
    registerRefNoOfSeedCertIssuedByNDA: "",
    batchNumber: "ONION2",
    purity: "90",
    geneticWorth: "",
    germinationRate: 90,
    seedPerGram: 7,
    packetsAvailable: 15,
    cost: 250
  }
];

export const seedings = [
  {
    nursery: "Nur1",
    grownFor: "Far1221",
    yearSeason: "2018",
    amtRequired: 150,
    seedLot: "00001",
    cropName: "White Onion",
    sowingComplete: "Y",
    type: "Onion",
    treePerBox: 150,
    totalGrown: 500,
    liftingComplete: "Y",
    treeCost: 300,
    nurserySeedlingId: "0000001",
    snowingDate: "01 / 12 / 2018"
  }
];

export const transaciton = [
  {
    seller: "Nur1",
    buyer: "Far12211",
    tradeDate: 15 / 12 / 2018,
    location: "Dahod",
    cropName: "Onion",
    quantity: 1,
    costSeedling: 300,
    costStorage: 150,
    costTotal: 450,
    seedLot: "00001",
    nurserySeedlingId: "0000001"
  }
];
