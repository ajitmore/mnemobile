import { Component, OnInit } from "@angular/core";
import { ListService } from "./list.service";

@Component({
  selector: "app-list",
  templateUrl: "list.page.html",
  styleUrls: ["list.page.scss"]
})
export class ListPage implements OnInit {
  // private selectedItem: any;
  products = [];
  // public items: Array<{ title: string; note: string; icon: string }> = [];
  constructor(private listService: ListService) {
    /*  for (let i = 1; i < 11; i++) {
      this.items.push({
        title: "Item " + i,
        note: "This is item #" + i
      });
    } */
  }

  ngOnInit() {
    this.listService.getProducts().subscribe((products: any) => {
      this.products = products.data;
    });
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
