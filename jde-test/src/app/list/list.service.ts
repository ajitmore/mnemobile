import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({ providedIn: "root" })
export class ListService {
  headers = new HttpHeaders();
  constructor(private httpClient: HttpClient) {
    this.headers.set("auth", "TW9uaXRvckFuZEV2YWx1YXRl");
    this.headers.set(
      "token",
      "69975c4c3cc0b0a78a527fc5b06629047639fed0998c7d9ba4b21d71ff8373bed73a3f5942e17e1a27f2ae804ba069ddb506b372d0157273b53b0363dad9909c"
    );
    this.headers.set("content-type", "application/json");
  }

  getProducts() {
    return this.httpClient.get(" https://mne-dbservice.herokuapp.com/seeds", {
      headers: this.headers
    });
  }
}
