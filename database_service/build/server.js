"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var helmet = require("helmet");
var logger = require("morgan");
var cors = require("cors");
var session = require("express-session");
// Add routes
var UserRouter_1 = require("./router/UserRouter");
var SeedRouter_1 = require("./router/SeedRouter");
var SeedlingRouter_1 = require("./router/SeedlingRouter");
var TransactionRouter_1 = require("./router/TransactionRouter");
var AuthenticatonRouter_1 = require("./router/AuthenticatonRouter");
// server class
var Server = /** @class */ (function () {
    function Server() {
        this.app = express();
        this.config();
        this.routes();
    }
    Server.prototype.config = function () {
        //Set up mongoose
        //const MONGO_URI = 'mongodb://localhost:27017/mnedb';
        var MONGO_URI = 'mongodb://ajitmore:Password_123@cluster0-shard-00-00-gkert.mongodb.net:27017,cluster0-shard-00-01-gkert.mongodb.net:27017,cluster0-shard-00-02-gkert.mongodb.net:27017/mnedb?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true';
        mongoose.connect(MONGO_URI || process.env.MONGO_URI);
        // config
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(helmet());
        this.app.use(logger('dev'));
        this.app.use(cors());
        //use sessions for tracking logins
        this.app.use(session({
            secret: 'agriculture',
            resave: true,
            saveUninitialized: false
        }));
    };
    Server.prototype.middleware = function (req, res, next) {
        console.log('middleware');
        // if(!req.headers.auth) {
        //     res.statusCode = 401;
        //     Common.sendError(res, Config.PLEASE_PROVIDE_AUTHENTICATION_TOKEN);
        // }
        // if(req.headers.auth === 'TW9uaXRvckFuZEV2YWx1YXRl') {
        //     if(req.path.indexOf("login") > -1 || req.path.indexOf("registration") > -1) {
        next();
        //     } 
        //     else {
        //         Authenticate.validateToken(req.headers.token)
        //         .then(data => {
        //             next()
        //         })
        //         .catch(err => {
        //             res.statusCode = 401;
        //             Common.sendError(res, err);
        //         })
        //     }
        // } else {
        //     res.sendStatus(401);
        // }
    };
    Server.prototype.routes = function () {
        var router;
        router = express.Router();
        this.app.use('/', this.middleware);
        this.app.use('/users', UserRouter_1.default);
        this.app.use('/seeds', SeedRouter_1.default);
        this.app.use('/seedlings', SeedlingRouter_1.default);
        this.app.use('/transactions', TransactionRouter_1.default);
        this.app.use('/auth', AuthenticatonRouter_1.default);
    };
    return Server;
}());
exports.default = new Server().app;
//# sourceMappingURL=server.js.map