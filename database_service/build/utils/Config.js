"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = /** @class */ (function () {
    function Config() {
    }
    Config.USERNAME_OR_PASSWORD_IS_NOT_PROVIDED = "User or Password is not provided";
    Config.USERNAME_IS_NOT_PROVIDED = "User is not provided";
    Config.USER_NOT_FOUND = "User is not found";
    Config.TOKEN_IS_NOT_PROVIDED = "Token is not provided";
    Config.PLEASE_PROVIDE_DETAILS = "Please provide details";
    Config.PLEASE_PROVIDE_AUTHENTICATION_TOKEN = "Please proivde authentication token";
    Config.INVALID_TOKEN = "Token is invalid";
    Config.PROVIDE_SEED_NUMBER = "Provide seed number";
    Config.PROVIDE_SEEDLING_NUMBER = "Provide seedling number";
    Config.PROVIDE_TRANSACTION_NUMBER = "Provide transaction number";
    return Config;
}());
//export
exports.default = Config;
//# sourceMappingURL=Config.js.map