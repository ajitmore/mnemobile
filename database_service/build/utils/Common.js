"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Common = /** @class */ (function () {
    function Common() {
    }
    Common.prototype.sendError = function (res, err) {
        var status = res.statusCode;
        res.json({
            status: status,
            err: err
        });
    };
    return Common;
}());
//export
exports.default = new Common();
//# sourceMappingURL=Common.js.map