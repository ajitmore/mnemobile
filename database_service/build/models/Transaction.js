"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var TransactionSchema = new mongoose_1.Schema({
    createdAt: Date,
    createdBy: mongoose_1.Schema.Types.ObjectId,
    updatedAt: Date,
    updatedBy: mongoose_1.Schema.Types.ObjectId,
    seller: {
        type: String,
        required: true
    },
    buyer: {
        type: String,
        required: true
    },
    tradeDate: {
        type: Date,
        required: true
    },
    location: String,
    cropName: String,
    quantity: Number,
    costSeedling: Number,
    costStorage: Number,
    costTotal: Number,
    seedLot: Number,
    nurserySeedlingId: Number
});
exports.default = mongoose_1.model('Transaction', TransactionSchema);
//# sourceMappingURL=Transaction.js.map