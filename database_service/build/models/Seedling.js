"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var SeedlingSchema = new mongoose_1.Schema({
    createdAt: Date,
    createdBy: mongoose_1.Schema.Types.ObjectId,
    updatedAt: Date,
    updatedBy: mongoose_1.Schema.Types.ObjectId,
    nursery: {
        type: String,
        required: true
    },
    grownFor: {
        type: String,
        required: true
    },
    yearSeason: {
        type: String,
        required: true
    },
    amtRequired: {
        type: Number,
        required: true
    },
    seedLot: {
        type: Number,
        required: true
    },
    cropName: {
        type: String,
        required: true
    },
    sowingComplete: {
        type: Boolean,
        required: true
    },
    type: String,
    size: String,
    age: Number,
    treePerBox: Number,
    totalGrown: Number,
    liftingComplete: Boolean,
    treeCost: Number,
    townShip: String,
    range: String,
    section: String,
    nurserySeedlingId: Number,
    creator: String,
    needed: Number,
    postSurplus: Boolean,
    overrideSurplus: Number,
    overrideSeason: Number,
    archieved: Boolean,
    fileAttachments: {
        type: Buffer
    },
    snowingDate: Date
});
exports.default = mongoose_1.model('Seedling', SeedlingSchema);
//# sourceMappingURL=Seedling.js.map