"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var SeedSchema = new mongoose_1.Schema({
    createdAt: Date,
    createdBy: mongoose_1.Schema.Types.ObjectId,
    updatedAt: Date,
    updatedBy: mongoose_1.Schema.Types.ObjectId,
    seedLot: {
        type: Number,
        required: true,
        unique: true
    },
    species: {
        type: String,
        required: true
    },
    seedClass: {
        type: String,
        default: '',
        required: true
    },
    seedZone: {
        type: String,
        default: '',
        required: true
    },
    basicMaterialType: String,
    regionOfProvenence: String,
    origin: String,
    registerRefNoOfSeedCertIssuedByNDA: String,
    batchNumber: String,
    purity: String,
    geneticWorth: String,
    germinationRate: Number,
    seedPerPacket: Number,
    packetsAvailable: Number,
    seedCost: Number
});
exports.default = mongoose_1.model('Seed', SeedSchema);
//# sourceMappingURL=Seed.js.map