"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Seed_1 = require("../models/Seed");
var Common_1 = require("../utils/Common");
var Config_1 = require("../utils/Config");
var SeedRouter = /** @class */ (function () {
    function SeedRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    SeedRouter.prototype.GetSeeds = function (req, res) {
        Seed_1.default.find({})
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    SeedRouter.prototype.GetSeed = function (req, res) {
        var _id = req.params.id;
        if (_id) {
            Seed_1.default.find({ _id: _id })
                .then(function (data) {
                var status = res.statusCode;
                res.json({
                    status: status,
                    data: data
                });
            })
                .catch(function (err) {
                Common_1.default.sendError(res, err);
            });
        }
    };
    SeedRouter.prototype.AddSeed = function (req, res) {
        var seedLot = req.body.seedLot, species = req.body.species, seedClass = req.body.seedClass, seedZone = req.body.seedZone, basicMaterialType = req.body.basicMaterialType, regionOfProvenence = req.body.regionOfProvenence, origin = req.body.origin, registerRefNoOfSeedCertIssuedByNDA = req.body.registerRefNoOfSeedCertIssuedByNDA, batchNumber = req.body.batchNumber, purity = req.body.purity, geneticWorth = req.body.geneticWorth, germinationRate = req.body.germinationRate, seedPerPacket = req.body.seedPerPacket, packetsAvailable = req.body.packetsAvailable, seedCost = req.body.seedCost;
        var seed = new Seed_1.default({
            seedLot: seedLot,
            species: species,
            seedClass: seedClass,
            seedZone: seedZone,
            basicMaterialType: basicMaterialType,
            regionOfProvenence: regionOfProvenence,
            origin: origin,
            registerRefNoOfSeedCertIssuedByNDA: registerRefNoOfSeedCertIssuedByNDA,
            batchNumber: batchNumber,
            purity: purity,
            geneticWorth: geneticWorth,
            germinationRate: germinationRate,
            seedPerPacket: seedPerPacket,
            packetsAvailable: packetsAvailable,
            seedCost: seedCost
        });
        seed
            .save()
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    SeedRouter.prototype.UpdateSeed = function (req, res) {
        var _id = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.PROVIDE_SEED_NUMBER);
        }
        Seed_1.default.findOneAndUpdate({ _id: _id }, req.body)
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    SeedRouter.prototype.DeleteSeed = function (req, res) {
        var _id = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.PROVIDE_SEED_NUMBER);
        }
        Seed_1.default.findOneAndRemove({ _id: _id })
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    SeedRouter.prototype.routes = function () {
        this.router.get("/", this.GetSeeds);
        this.router.get("/:id", this.GetSeed);
        this.router.post("/", this.AddSeed);
        this.router.put("/:id", this.UpdateSeed);
        this.router.delete("/:id", this.DeleteSeed);
    };
    return SeedRouter;
}());
//export
var seedRoutes = new SeedRouter();
seedRoutes.routes();
exports.default = seedRoutes.router;
//# sourceMappingURL=SeedRouter.js.map