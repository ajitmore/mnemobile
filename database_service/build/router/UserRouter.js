"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var User_1 = require("../models/User");
var Common_1 = require("../utils/Common");
var Config_1 = require("../utils/Config");
var UserRouter = /** @class */ (function () {
    function UserRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    UserRouter.prototype.GetUsers = function (req, res) {
        User_1.default.find({})
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    UserRouter.prototype.GetUser = function (req, res) {
        var username = (req.params.username) ? req.params.username : null;
        if (!username) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.USERNAME_IS_NOT_PROVIDED);
        }
        User_1.default.findOne({ username: username })
            .populate("title")
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    UserRouter.prototype.CreateUser = function (req, res) {
        var name = req.body.name;
        var email = req.body.email;
        var username = req.body.username;
        var password = req.body.password;
        if (!name || !email || !username || !password) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.PLEASE_PROVIDE_DETAILS);
        }
        var user = new User_1.default({
            name: name,
            email: email,
            username: username,
            password: password
        });
        user
            .save()
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    UserRouter.prototype.UpdateUser = function (req, res) {
        var username = (req.params.username) ? req.params.username : null;
        if (!username) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.USERNAME_IS_NOT_PROVIDED);
        }
        User_1.default.findOneAndUpdate({ username: username }, req.body)
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    UserRouter.prototype.DeleteUser = function (req, res) {
        var username = (req.params.username) ? req.params.username : null;
        if (!username) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.USERNAME_IS_NOT_PROVIDED);
        }
        User_1.default.findOneAndRemove({ username: username })
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    UserRouter.prototype.routes = function () {
        this.router.get("/", this.GetUsers);
        this.router.get("/:username", this.GetUser);
        this.router.post("/registration", this.CreateUser);
        this.router.put("/:username", this.UpdateUser);
        this.router.delete("/:username", this.DeleteUser);
    };
    return UserRouter;
}());
//export
var userRoutes = new UserRouter();
userRoutes.routes();
exports.default = userRoutes.router;
//# sourceMappingURL=UserRouter.js.map