"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Transaction_1 = require("../models/Transaction");
var Common_1 = require("../utils/Common");
var Config_1 = require("../utils/Config");
var TransactionRouter = /** @class */ (function () {
    function TransactionRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    TransactionRouter.prototype.GetTransactions = function (req, res) {
        Transaction_1.default.find({})
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    TransactionRouter.prototype.GetTransaction = function (req, res) {
        var _id = req.params.id;
        if (_id) {
            Transaction_1.default.find({ _id: _id })
                .then(function (data) {
                var status = res.statusCode;
                res.json({
                    status: status,
                    data: data
                });
            })
                .catch(function (err) {
                Common_1.default.sendError(res, err);
            });
        }
    };
    TransactionRouter.prototype.AddTransaction = function (req, res) {
        var seller = req.body.seller, buyer = req.body.buyer, tradeDate = req.body.tradeDate, location = req.body.location, cropName = req.body.cropName, quantity = req.body.quantity, costSeedling = req.body.costSeedling, costSeed = req.body.costSeed, costStorage = req.body.costStorage, costTotal = req.body.costTotal;
        var transaction = new Transaction_1.default({
            seller: seller,
            buyer: buyer,
            tradeDate: tradeDate,
            location: location,
            cropName: cropName,
            quantity: quantity,
            costSeedling: costSeedling,
            costSeed: costSeed,
            costStorage: costStorage,
            costTotal: costTotal,
        });
        transaction
            .save()
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    TransactionRouter.prototype.UpdateTransaction = function (req, res) {
        var _id = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.PROVIDE_SEED_NUMBER);
        }
        Transaction_1.default.findOneAndUpdate({ _id: _id }, req.body)
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    TransactionRouter.prototype.DeleteTransaction = function (req, res) {
        var _id = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.PROVIDE_SEED_NUMBER);
        }
        Transaction_1.default.findOneAndRemove({ _id: _id })
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    TransactionRouter.prototype.routes = function () {
        this.router.get("/", this.GetTransactions);
        this.router.get("/:id", this.GetTransaction);
        this.router.post("/", this.AddTransaction);
        this.router.put("/:id", this.UpdateTransaction);
        this.router.delete("/:id", this.DeleteTransaction);
    };
    return TransactionRouter;
}());
//export
var transactionRoutes = new TransactionRouter();
transactionRoutes.routes();
exports.default = transactionRoutes.router;
//# sourceMappingURL=TransactionRouter.js.map