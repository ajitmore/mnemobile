"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var User_1 = require("../models/User");
var Common_1 = require("../utils/Common");
var bcrypt = require("bcrypt");
var crypto = require("crypto");
var Config_1 = require("../utils/Config");
var AuthenticationRouter = /** @class */ (function () {
    function AuthenticationRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    /**
       * LoginUser
  string: username, string: password     */
    AuthenticationRouter.prototype.LoginUser = function (req, res) {
        var username = req.headers.username ? req.headers.username : null, password = req.headers.password ? req.headers.password : null;
        if (!username || !password) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.USERNAME_OR_PASSWORD_IS_NOT_PROVIDED);
        }
        User_1.default.findOne({ username: username })
            .then(function (data) {
            if (!data) {
                res.statusCode = 401;
                Common_1.default.sendError(res, Config_1.default.USER_NOT_FOUND);
            }
            bcrypt.compare(password, data.password, function (err, result) {
                if (result) {
                    var token_1 = crypto.randomBytes(64).toString("hex");
                    var body = {
                        token: token_1
                    };
                    User_1.default.findOneAndUpdate({ username: username }, body).then(function (data) {
                        if (data) {
                            data = {
                                email: data.email,
                                token: token_1
                            };
                            var status_1 = res.statusCode;
                            res.json({
                                status: status_1,
                                data: data
                            });
                        }
                    });
                }
                else {
                    res.json({
                        status: status
                    });
                }
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    AuthenticationRouter.prototype.Logout = function (req, res) {
        var reqBody = {
            token: ""
        };
        var username = req.headers.username ? req.headers.username : null;
        if (!username) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.USERNAME_IS_NOT_PROVIDED);
        }
        User_1.default.findOneAndUpdate({ username: username }, reqBody)
            .then(function (data) {
            if (data) {
                var status_2 = res.statusCode;
                res.json({
                    status: status_2,
                    data: data
                });
            }
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    AuthenticationRouter.prototype.AuthenticateUser = function (req, res) {
        var token = req.headers.token;
        if (!token) {
            res.statusCode = 401;
            Common_1.default.sendError(res, Config_1.default.TOKEN_IS_NOT_PROVIDED);
        }
        User_1.default.findOne({ token: token })
            .then(function (data) {
            if (data) {
                res.send(200);
            }
            else {
                res.send(401);
            }
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    AuthenticationRouter.prototype.routes = function () {
        this.router.post("/login", this.LoginUser);
        this.router.post("/logout", this.Logout);
        //this.router.post("/validate", this.AuthenticateUser);
    };
    return AuthenticationRouter;
}());
//export
var authRoutes = new AuthenticationRouter();
authRoutes.routes();
exports.default = authRoutes.router;
//# sourceMappingURL=AuthenticatonRouter.js.map