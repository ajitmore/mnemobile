"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Seedling_1 = require("../models/Seedling");
var Common_1 = require("../utils/Common");
var Config_1 = require("../utils/Config");
var SeedlingRouter = /** @class */ (function () {
    function SeedlingRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    SeedlingRouter.prototype.GetSeedlings = function (req, res) {
        Seedling_1.default.find({})
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    SeedlingRouter.prototype.GetSeedling = function (req, res) {
        var _id = req.params.id;
        if (_id) {
            Seedling_1.default.find({ _id: _id })
                .then(function (data) {
                var status = res.statusCode;
                res.json({
                    status: status,
                    data: data
                });
            })
                .catch(function (err) {
                Common_1.default.sendError(res, err);
            });
        }
    };
    SeedlingRouter.prototype.AddSeedling = function (req, res) {
        var nursery = req.body.nursery, grownFor = req.body.grownFor, yearSeason = req.body.yearSeason, amtRequired = req.body.amtRequired, seedLot = req.body.seedLot, cropName = req.body.cropName, sowingComplete = req.body.sowingComplete, type = req.body.type, size = req.body.size, age = req.body.age, treePerBox = req.body.treePerBox, totalGrown = req.body.totalGrown, liftingComplete = req.body.liftingComplete, treeCost = req.body.treeCost, townShip = req.body.townShip, range = req.body.range, section = req.body.section, nurserySeedlingId = req.body.nurserySeedlingId, creator = req.body.creator, needed = req.body.needed, postSurplus = req.body.postSurplus, overrideSurplus = req.body.overrideSurplus, overrideSeason = req.body.overrideSeason, archieved = req.body.archieved, fileAttachments = req.body.fileAttachments, snowingDate = req.body.snowingDate;
        var seedling = new Seedling_1.default({
            nursery: nursery,
            grownFor: grownFor,
            yearSeason: yearSeason,
            amtRequired: amtRequired,
            seedLot: seedLot,
            cropName: cropName,
            sowingComplete: sowingComplete,
            type: type,
            size: size,
            age: age,
            treePerBox: treePerBox,
            totalGrown: totalGrown,
            liftingComplete: liftingComplete,
            treeCost: treeCost,
            townShip: townShip,
            range: range,
            section: section,
            nurserySeedlingId: nurserySeedlingId,
            creator: creator,
            needed: needed,
            postSurplus: postSurplus,
            overrideSurplus: overrideSurplus,
            overrideSeason: overrideSeason,
            archieved: archieved,
            fileAttachments: fileAttachments,
            snowingDate: snowingDate
        });
        seedling
            .save()
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    SeedlingRouter.prototype.UpdateSeedling = function (req, res) {
        var _id = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.PROVIDE_SEED_NUMBER);
        }
        Seedling_1.default.findOneAndUpdate({ _id: _id }, req.body)
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    SeedlingRouter.prototype.DeleteSeedling = function (req, res) {
        var _id = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common_1.default.sendError(res, Config_1.default.PROVIDE_SEED_NUMBER);
        }
        Seedling_1.default.findOneAndRemove({ _id: _id })
            .then(function (data) {
            var status = res.statusCode;
            res.json({
                status: status,
                data: data
            });
        })
            .catch(function (err) {
            Common_1.default.sendError(res, err);
        });
    };
    SeedlingRouter.prototype.routes = function () {
        this.router.get("/", this.GetSeedlings);
        this.router.get("/:id", this.GetSeedling);
        this.router.post("/", this.AddSeedling);
        this.router.put("/:id", this.UpdateSeedling);
        this.router.delete("/:id", this.DeleteSeedling);
    };
    return SeedlingRouter;
}());
//export
var seedlingRoutes = new SeedlingRouter();
seedlingRoutes.routes();
exports.default = seedlingRoutes.router;
//# sourceMappingURL=SeedlingRouter.js.map