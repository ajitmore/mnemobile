import { Schema, model } from "mongoose";
import { ObjectID } from "bson";

let SeedlingSchema: Schema = new Schema({
    createdAt: Date,
    createdBy: Schema.Types.ObjectId,
    updatedAt: Date,
    updatedBy: Schema.Types.ObjectId,
    nursery: {
        type: String,
        required: true
    },
    grownFor: {
        type: String,
        required: true
    },
    yearSeason: {
        type: String,
        required: true
    },
    amtRequired: {
        type: Number,
        required: true
    },
    seedLot: {
        type: Number,
        required: true
    },
    cropName: {
        type: String,
        required: true
    },
    sowingComplete: {
        type: Boolean,
        required: true
    },
    type: String,
    size: String,
    age: Number,
    treePerBox: Number,
    totalGrown: Number,
    liftingComplete: Boolean,
    treeCost: Number,
    townShip: String,
    range: String,
    section: String,
    nurserySeedlingId: Number,
    creator: String,
    needed: Number,
    postSurplus: Boolean,
    overrideSurplus: Number,
    overrideSeason: Number,
    archieved: Boolean,
    fileAttachments: {
        type: Buffer
    },
    snowingDate: Date
});

export default model('Seedling', SeedlingSchema);