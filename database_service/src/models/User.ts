import { Schema, model } from "mongoose";
import * as bcrypt from "bcrypt";

var UserSchema: Schema = new Schema({
  createdAt: Date,
  updatedAt: Date,
  name: {
    type: String,
    default: "",
    required: true
  },
  email: {
    type: String,
    default: "",
    required: true,
    unique: true,
    trim: true
  },
  username: {
    type: String,
    default: "",
    required: true,
    unique: true,
    lowercase: true,
    trim: true
  },
  password: {
    type: String,
    default: "",
    required: true
  },
  token: {
      type: String,
      default: ""
  }
});

//hashing a password before saving it to the database
UserSchema.pre("save", function(next) {
  let user: any = this;
  bcrypt.hash(user.password, 10, function(err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  });
});


export default model("User", UserSchema);
