import { Schema, model } from "mongoose";
import { ObjectID } from "bson";

let SeedSchema: Schema = new Schema({
    createdAt: Date,
    createdBy: Schema.Types.ObjectId,
    updatedAt: Date,
    updatedBy: Schema.Types.ObjectId,
    seedLot: {
        type: Number,
        required: true,
        unique: true
    },
    species: {
        type: String,
        required: true
    },
    seedClass: {
        type: String,
        default: '',
        required: true
    },
    seedZone: {
        type: String,
        default: '',
        required: true
    },
    basicMaterialType: String,
    regionOfProvenence: String,
    origin: String,
    registerRefNoOfSeedCertIssuedByNDA: String,
    batchNumber: String,
    purity: String,
    geneticWorth: String,
    germinationRate: Number,
    seedPerPacket: Number,
    packetsAvailable: Number,
    seedCost: Number
});

export default model('Seed', SeedSchema);