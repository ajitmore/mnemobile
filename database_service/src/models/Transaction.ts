import { Schema, model } from "mongoose";
import { ObjectID } from "bson";

let TransactionSchema: Schema = new Schema({
    createdAt: Date,
    createdBy: Schema.Types.ObjectId,
    updatedAt: Date,
    updatedBy: Schema.Types.ObjectId,
    seller: {
        type: String,
        required: true
    },
    buyer: {
        type: String,
        required: true
    },
    tradeDate: {
        type: Date,
        required: true
    },
    location: String,
    cropName: String,
    quantity: Number,
    costSeedling: Number,
    costStorage: Number,
    costTotal: Number,
    seedLot: Number,
    nurserySeedlingId: Number
});

export default model('Transaction', TransactionSchema);