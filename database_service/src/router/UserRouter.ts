import { Router, Request, Response, NextFunction } from "express";
import User from "../models/User";
import Common from "../utils/Common";
import Config from "../utils/Config";
import * as bcrypt from "bcrypt";
import * as crypto from "crypto";

class UserRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  public GetUsers(req: Request, res: Response): void {
    User.find({})
      .then(data => {
        const status = res.statusCode;
        res.json({
          status,
          data
        });
      })
      .catch(err => {
        Common.sendError(res, err);
      });
  }

  public GetUser(req: Request, res: Response): void {
    const username: string = (req.params.username) ? req.params.username: null;
    if(!username) {
        res.statusCode = 400;
        Common.sendError(res, Config.USERNAME_IS_NOT_PROVIDED);
    }
    User.findOne({ username })
      .populate("title")
      .then(data => {
        const status = res.statusCode;
        res.json({
          status,
          data
        });
      })
      .catch(err => {
        Common.sendError(res, err);
      });
  }

  public CreateUser(req: Request, res: Response): void {
    const name: string = req.body.name;
    const email: string = req.body.email;
    const username: string = req.body.username;
    const password: string = req.body.password;

    if (!name || !email || !username || !password) {
      res.statusCode = 400;
      Common.sendError(res, Config.PLEASE_PROVIDE_DETAILS);
    }

    const user = new User({
      name,
      email,
      username,
      password
    });

    user
      .save()
      .then(data => {
        const status = res.statusCode;
        res.json({
          status,
          data
        });
      })
      .catch(err => {        
        Common.sendError(res, err);
      });
  }

  public UpdateUser(req: Request, res: Response): void {
    const username: string = (req.params.username) ? req.params.username: null;
    if(!username) {
        res.statusCode = 400;
        Common.sendError(res, Config.USERNAME_IS_NOT_PROVIDED);
    }
    User.findOneAndUpdate({ username }, req.body)
      .then(data => {
        const status = res.statusCode;
        res.json({
          status,
          data
        });
      })
      .catch(err => {
        Common.sendError(res, err);
      });
  }

  public DeleteUser(req: Request, res: Response): void {
    const username: string = (req.params.username) ? req.params.username: null;
    if(!username) {
        res.statusCode = 400;
        Common.sendError(res, Config.USERNAME_IS_NOT_PROVIDED);
    }
    User.findOneAndRemove({ username })
      .then(data => {
          const status = res.statusCode;
          res.json({
            status,
            data
          });
      })
      .catch(err => {
          Common.sendError(res, err);
      });
  }

  routes() {
    this.router.get("/", this.GetUsers);
    this.router.get("/:username", this.GetUser);
    this.router.post("/registration", this.CreateUser);
    this.router.put("/:username", this.UpdateUser);
    this.router.delete("/:username", this.DeleteUser);    
  }
}

//export
const userRoutes = new UserRouter();
userRoutes.routes();

export default userRoutes.router;
