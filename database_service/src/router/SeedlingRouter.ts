import { Router, Request, Response, NextFunction } from "express";
import Seedling from "../models/Seedling";
import Common from "../utils/Common";
import Config from "../utils/Config";
import { ObjectId } from "bson";

class SeedlingRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public GetSeedlings(req: Request, res: Response) {
        Seedling.find({})
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                })
            })
            .catch(err => {
                Common.sendError(res, err);
            })
    }

    public GetSeedling(req: Request, res: Response) {
        const _id: ObjectId = req.params.id;
        if (_id) {
            Seedling.find({ _id: _id })
                .then(data => {
                    const status = res.statusCode;
                    res.json({
                        status,
                        data
                    })
                })
                .catch(err => {
                    Common.sendError(res, err);
                })
        }
    }

    public AddSeedling(req: Request, res: Response) {
        const nursery: String = req.body.nursery,
            grownFor: String = req.body.grownFor,
            yearSeason: String = req.body.yearSeason,
            amtRequired: Number = req.body.amtRequired,
            seedLot: Number = req.body.seedLot,
            cropName: String = req.body.cropName,
            sowingComplete: Boolean = req.body.sowingComplete,
            type: String = req.body.type,
            size: String = req.body.size,
            age: Number = req.body.age,
            treePerBox: Number = req.body.treePerBox,
            totalGrown: Number = req.body.totalGrown,
            liftingComplete: Boolean = req.body.liftingComplete,
            treeCost: Number = req.body.treeCost,
            townShip: String = req.body.townShip,
            range: String = req.body.range,
            section: String = req.body.section,
            nurserySeedlingId: Number = req.body.nurserySeedlingId,
            creator: String = req.body.creator,
            needed: Number = req.body.needed,
            postSurplus: Boolean = req.body.postSurplus,
            overrideSurplus: Number = req.body.overrideSurplus,
            overrideSeason: Number = req.body.overrideSeason,
            archieved: Boolean = req.body.archieved,
            fileAttachments: Buffer = req.body.fileAttachments,
            snowingDate: Date = req.body.snowingDate

        const seedling = new Seedling({
            nursery,
            grownFor,
            yearSeason,
            amtRequired,
            seedLot,
            cropName,
            sowingComplete,
            type,
            size,
            age,
            treePerBox,
            totalGrown,
            liftingComplete,
            treeCost,
            townShip,
            range,
            section,
            nurserySeedlingId,
            creator,
            needed,
            postSurplus,
            overrideSurplus,
            overrideSeason,
            archieved,
            fileAttachments,
            snowingDate
        });

        seedling
            .save()
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch(err => {
                Common.sendError(res, err);
            });
    }

    public UpdateSeedling(req: Request, res: Response) {
        const _id: string = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common.sendError(res, Config.PROVIDE_SEED_NUMBER);
        }
        Seedling.findOneAndUpdate({ _id: _id }, req.body)
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch(err => {
                Common.sendError(res, err);
            });
    }

    public DeleteSeedling(req: Request, res: Response) {
        const _id: string = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common.sendError(res, Config.PROVIDE_SEED_NUMBER);
        }
        Seedling.findOneAndRemove({ _id: _id })
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch(err => {
                Common.sendError(res, err);
            });
    }

    routes() {
        this.router.get("/", this.GetSeedlings);
        this.router.get("/:id", this.GetSeedling);
        this.router.post("/", this.AddSeedling);
        this.router.put("/:id", this.UpdateSeedling);
        this.router.delete("/:id", this.DeleteSeedling);
    }
}

//export
const seedlingRoutes = new SeedlingRouter();
seedlingRoutes.routes();

export default seedlingRoutes.router;