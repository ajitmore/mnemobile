import { Router, Request, Response, NextFunction } from "express";
import User from "../models/User";
import Common from "../utils/Common";
import * as bcrypt from "bcrypt";
import * as crypto from "crypto";
import Config from "../utils/Config";

class AuthenticationRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  /**
     * LoginUser
string: username, string: password     */
  public LoginUser(req: Request, res: Response) {
    let username = req.headers.username ? req.headers.username : null,
      password = req.headers.password ? req.headers.password : null;
    if (!username || !password) {
      res.statusCode = 400;
      Common.sendError(res, Config.USERNAME_OR_PASSWORD_IS_NOT_PROVIDED);
    }
    User.findOne({ username: username })
      .then((data: any) => {
        if (!data) {
          res.statusCode = 401;
          Common.sendError(res, Config.USER_NOT_FOUND);
        }

        bcrypt.compare(password, data.password, function(err, result) {
          if (result) {
            let token = crypto.randomBytes(64).toString("hex");
            var body = {
              token: token
            };
            User.findOneAndUpdate({ username }, body).then((data: any) => {
              if (data) {
                data = {
                  email: data.email,
                  token: token
                };
                const status = res.statusCode;
                res.json({
                  status,
                  data
                });
              }
            });
          } else {
            res.json({
              status
            });
          }
        });
      })
      .catch(err => {
        Common.sendError(res, err);
      });
  }

  public Logout(req: Request, res: Response) {
    let reqBody = {
      token: ""
    };
    let username = req.headers.username ? req.headers.username : null;
    if (!username) {
      res.statusCode = 400;
      Common.sendError(res, Config.USERNAME_IS_NOT_PROVIDED);
    }
    User.findOneAndUpdate({ username }, reqBody)
      .then(data => {
        if (data) {
          let status = res.statusCode;
          res.json({
            status,
            data
          });
        }
      })
      .catch(err => {
        Common.sendError(res, err);
      });
  }

  public AuthenticateUser(req: Request, res: Response) {
    let token = req.headers.token;
    if (!token) {
      res.statusCode = 401;
      Common.sendError(res, Config.TOKEN_IS_NOT_PROVIDED);
    }
    
    User.findOne({ token: token })
    .then(data => {
        if (data) {
            res.send(200);
        } else {
            res.send(401);
        }
    })
    .catch(err => {
        Common.sendError(res, err);
    });
  }

  routes() {
    this.router.post("/login", this.LoginUser);
    this.router.post("/logout", this.Logout);
    //this.router.post("/validate", this.AuthenticateUser);
  }
}

//export
const authRoutes = new AuthenticationRouter();
authRoutes.routes();

export default authRoutes.router;
