import { Router, Request, Response, NextFunction } from "express";
import Seed from "../models/Seed";
import Common from "../utils/Common";
import Config from "../utils/Config";
import { ObjectId } from "bson";

class SeedRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public GetSeeds(req: Request, res: Response) {
        Seed.find({})
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                })
            })
            .catch(err => {
                Common.sendError(res, err);
            })
    }

    public GetSeed(req: Request, res: Response) {
        const _id: ObjectId = req.params.id;
        if (_id) {
            Seed.find({ _id: _id })
                .then(data => {
                    const status = res.statusCode;
                    res.json({
                        status,
                        data
                    })
                })
                .catch(err => {
                    Common.sendError(res, err);
                })
        }
    }

    public AddSeed(req: Request, res: Response) {
        const seedLot: number = req.body.seedLot,
            species: string = req.body.species,
            seedClass: string = req.body.seedClass,
            seedZone: string = req.body.seedZone,
            basicMaterialType: String = req.body.basicMaterialType,
            regionOfProvenence: String = req.body.regionOfProvenence,
            origin: String = req.body.origin,
            registerRefNoOfSeedCertIssuedByNDA: String = req.body.registerRefNoOfSeedCertIssuedByNDA,
            batchNumber: String = req.body.batchNumber,
            purity: String = req.body.purity,
            geneticWorth: String = req.body.geneticWorth,
            germinationRate: Number = req.body.germinationRate,
            seedPerPacket: Number = req.body.seedPerPacket,
            packetsAvailable: Number = req.body.packetsAvailable,
            seedCost: Number = req.body.seedCost

        const seed = new Seed({
            seedLot,
            species,
            seedClass,
            seedZone,
            basicMaterialType,
            regionOfProvenence,
            origin,
            registerRefNoOfSeedCertIssuedByNDA,
            batchNumber,
            purity,
            geneticWorth,
            germinationRate,
            seedPerPacket,
            packetsAvailable,
            seedCost
        });

        seed
            .save()
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch(err => {
                Common.sendError(res, err);
            });
    }

    public UpdateSeed(req: Request, res: Response) {
        const _id: string = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common.sendError(res, Config.PROVIDE_SEED_NUMBER);
        }
        Seed.findOneAndUpdate({ _id: _id }, req.body)
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch(err => {
                Common.sendError(res, err);
            });
    }

    public DeleteSeed(req: Request, res: Response) {
        const _id: string = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common.sendError(res, Config.PROVIDE_SEED_NUMBER);
        }
        Seed.findOneAndRemove({ _id: _id })
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch(err => {
                Common.sendError(res, err);
            });
    }

    routes() {
        this.router.get("/", this.GetSeeds);
        this.router.get("/:id", this.GetSeed);
        this.router.post("/", this.AddSeed);
        this.router.put("/:id", this.UpdateSeed);
        this.router.delete("/:id", this.DeleteSeed);
    }
}

//export
const seedRoutes = new SeedRouter();
seedRoutes.routes();

export default seedRoutes.router;