import { Router, Request, Response, NextFunction } from "express";
import Transaction from "../models/Transaction";
import Common from "../utils/Common";
import Config from "../utils/Config";
import { ObjectId } from "bson";

class TransactionRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public GetTransactions(req: Request, res: Response) {
        Transaction.find({})
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                })
            })
            .catch(err => {
                Common.sendError(res, err);
            })
    }

    public GetTransaction(req: Request, res: Response) {
        const _id: ObjectId = req.params.id;
        if (_id) {
            Transaction.find({ _id: _id })
                .then(data => {
                    const status = res.statusCode;
                    res.json({
                        status,
                        data
                    })
                })
                .catch(err => {
                    Common.sendError(res, err);
                })
        }
    }

    public AddTransaction(req: Request, res: Response) {
        const seller: String = req.body.seller,
            buyer: String = req.body.buyer,
            tradeDate: Date = req.body.tradeDate,
            location: String = req.body.location,
            cropName: String = req.body.cropName,
            quantity: Number = req.body.quantity,
            costSeedling: Number = req.body.costSeedling,
            costSeed: Number = req.body.costSeed,
            costStorage: Number = req.body.costStorage,
            costTotal: Number = req.body.costTotal;

        const transaction = new Transaction({
            seller,
            buyer,
            tradeDate,
            location,
            cropName,
            quantity,
            costSeedling,
            costSeed,
            costStorage,
            costTotal,
        });

        transaction
        .save()
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch(err => {
                Common.sendError(res, err);
            });
    }

    public UpdateTransaction(req: Request, res: Response) {
        const _id: string = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common.sendError(res, Config.PROVIDE_SEED_NUMBER);
        }
        Transaction.findOneAndUpdate({ _id: _id }, req.body)
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch(err => {
                Common.sendError(res, err);
            });
    }

    public DeleteTransaction(req: Request, res: Response) {
        const _id: string = (req.params.id) ? req.params.id : null;
        if (!_id) {
            res.statusCode = 400;
            Common.sendError(res, Config.PROVIDE_SEED_NUMBER);
        }
        Transaction.findOneAndRemove({ _id: _id })
            .then(data => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch(err => {
                Common.sendError(res, err);
            });
    }

    routes() {
        this.router.get("/", this.GetTransactions);
        this.router.get("/:id", this.GetTransaction);
        this.router.post("/", this.AddTransaction);
        this.router.put("/:id", this.UpdateTransaction);
        this.router.delete("/:id", this.DeleteTransaction);
    }
}

//export
const transactionRoutes = new TransactionRouter();
transactionRoutes.routes();

export default transactionRoutes.router;