import * as express from 'express';
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import * as logger from 'morgan';
import * as compression from 'compression';
import * as cors from 'cors';
import { Router, Request, Response, NextFunction } from "express";
import  * as session from "express-session";

// Add routes
import UserRouter from './router/UserRouter';
import SeedRouter from "./router/SeedRouter";
import SeedlingRouter from "./router/SeedlingRouter";
import TransactionRouter from "./router/TransactionRouter";
import AuthenticatonRouter from "./router/AuthenticatonRouter";


import Common from "./utils/Common";
import Config from './utils/Config';
import Authentication from './utils/Authenticate';

import { request } from 'http';
import Authenticate from './utils/Authenticate';

// server class

class Server {

    public app: express.Application;


    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    public config() {
        //Set up mongoose
        //const MONGO_URI = 'mongodb://localhost:27017/mnedb';
        const MONGO_URI =  'mongodb://ajitmore:Password_123@cluster0-shard-00-00-gkert.mongodb.net:27017,cluster0-shard-00-01-gkert.mongodb.net:27017,cluster0-shard-00-02-gkert.mongodb.net:27017/mnedb?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true';
        mongoose.connect(MONGO_URI || process.env.MONGO_URI);

        // config
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(helmet());
        this.app.use(logger('dev'));
        this.app.use(cors());

        //use sessions for tracking logins
        this.app.use(session({
            secret: 'agriculture',
            resave: true,
            saveUninitialized: false
        }));
    }

    private middleware(req: Request, res: Response, next: NextFunction): void {
        console.log('middleware');        
        // if(!req.headers.auth) {
        //     res.statusCode = 401;
        //     Common.sendError(res, Config.PLEASE_PROVIDE_AUTHENTICATION_TOKEN);
        // }
        // if(req.headers.auth === 'TW9uaXRvckFuZEV2YWx1YXRl') {
        //     if(req.path.indexOf("login") > -1 || req.path.indexOf("registration") > -1) {
                 next();
        //     } 
        //     else {
        //         Authenticate.validateToken(req.headers.token)
        //         .then(data => {
        //             next()
        //         })
        //         .catch(err => {
        //             res.statusCode = 401;
        //             Common.sendError(res, err);
        //         })
        //     }
            
        // } else {
        //     res.sendStatus(401);
        // }
    }

    public routes(): void {
        let router: express.Router;
        router = express.Router();

        this.app.use('/', this.middleware);
        this.app.use('/users', UserRouter);
        this.app.use('/seeds', SeedRouter);
        this.app.use('/seedlings', SeedlingRouter);
        this.app.use('/transactions', TransactionRouter);
        this.app.use('/auth', AuthenticatonRouter);
    }
}

export default new Server().app;