import { Request, Response } from "express";

class Common {
    public sendError(res: Response, err: any) {
        const status = res.statusCode;
        res.json({
          status,
          err
        });
    }
}

//export
export default new Common();