import User from "../models/User";

class Authenticate {
  constructor() {}
  public  async validateToken(token: any): Promise<any> {
    try {
      return new Promise((resolve, reject) => {
        User.findOne({ token: token })
          .then(data => {
            if (data) {
              resolve();
            } else {
              reject();
            }
          })
          .catch(err => {
            reject(err);
          });
      });
      //await User.findOne({ token: token });
    } catch (error) {}
  }
}
export default new Authenticate();
