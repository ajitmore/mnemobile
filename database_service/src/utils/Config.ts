class Config {
    public static USERNAME_OR_PASSWORD_IS_NOT_PROVIDED = "User or Password is not provided";
    public static USERNAME_IS_NOT_PROVIDED = "User is not provided";
    public static USER_NOT_FOUND = "User is not found";
    public static TOKEN_IS_NOT_PROVIDED = "Token is not provided";
    public static PLEASE_PROVIDE_DETAILS = "Please provide details";
    public static PLEASE_PROVIDE_AUTHENTICATION_TOKEN = "Please proivde authentication token";
    public static INVALID_TOKEN = "Token is invalid";
    public static PROVIDE_SEED_NUMBER = "Provide seed number"
    public static PROVIDE_SEEDLING_NUMBER = "Provide seedling number"
    public static PROVIDE_TRANSACTION_NUMBER = "Provide transaction number"
}

//export
export default Config;
