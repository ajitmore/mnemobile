enum MethodType {
    POST,
    GET,
    PUT,
    DELETE
}

export class Config {

    public static MethodType = MethodType;

    public static DB_SERVICE = {
        "BASE_URL": "http://localhost:3000/",
        "Login": "auth/login"
    }
    public static DB_SERVICE_BASE_URL = 'http:localhost:3000/';

    public static USERNAME_OR_PASSWORD_IS_NOT_PROVIDED = "User or Password is not provided";
    public static USERNAME_IS_NOT_PROVIDED = "User is not provided";
    public static USER_NOT_FOUND = "User is not found";
    public static TOKEN_IS_NOT_PROVIDED = "Token is not provided";
    public static PLEASE_PROVIDE_DETAILS = "Please provide details";
    public static PLEASE_PROVIDE_AUTHENTICATION_TOKEN = "Please proivde authentication token";
    public static INVALID_TOKEN = "Token is invalid"
}
