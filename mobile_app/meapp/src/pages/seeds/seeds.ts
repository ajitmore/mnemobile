import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { BarcodeScanner } from "@ionic-native/barcode-scanner";
/**
 * Generated class for the SeedsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-seeds',
    templateUrl: 'seeds.html',
})
export class SeedsPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SeedsPage');
    }

    public scanSeed() {
        this.barcodeScanner.scan()
        .then(barcodeData => {
            
        })
    }

}
