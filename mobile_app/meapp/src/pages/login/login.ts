import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, IonicPage, Keyboard } from 'ionic-angular';
import { LoginServiceProvider } from '../../providers/login-service/login-service';
import { HomePage } from '../home/home';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorProvider } from '../../providers/validator/validator';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  showFooter: boolean = true;
  loading: Loading;
  registerCredentials = { email: '', password: '' };
  emailRegularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  errmsg: string;

  credentialsForm: FormGroup;

  constructor(public keyboard: Keyboard, private formBuilder: FormBuilder, private nav: NavController, private loginService: LoginServiceProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {

    this.credentialsForm = this.formBuilder.group({

      email: [
        '', Validators.compose([
          Validators.pattern(ValidatorProvider.regexValidators.email),
          Validators.required
        ])
      ],
      password: [
        '', Validators.compose([
          //Validators.pattern(ValidatorProvider.regexValidators.password),
          Validators.required
        ])
      ]
    });

    this.registerCredentials.email = "ajit.more.mt@gmail.com";
    this.registerCredentials.password = "password@123";
  }

  public login() {
    if (this.credentialsForm.valid) {
      this.showLoading()

      this.registerCredentials.email = "ajit.more.mt@gmail.com";
      this.registerCredentials.password = "password@123";

      this.loginService.login(this.registerCredentials).subscribe(allowed => {
        if (allowed) {
          this.nav.push(HomePage);
        } else {
          this.showError("Invalid email or password");
        }
      },
        error => {
          this.showError(error);
        });
    }
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goBack() {
    this.nav.pop();
  }

  keyboardCheck() {
    return !this.keyboard.isOpen();
  }

}
