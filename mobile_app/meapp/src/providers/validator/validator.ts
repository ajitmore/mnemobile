import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

/*
  Generated class for the ValidatorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ValidatorProvider {
  
    // The Angular email validator accepts an email like "rob@example", perhaps because "rob@localhost" could be valid.
    // The pureEmail regex does not accept "ryan@example" as a valid email address, which I think is a good thing.
    // See: EMAIL_REGEXP from https://github.com/angular/angular.js/blob/ffb6b2fb56d9ffcb051284965dd538629ea9687a/src/ng/directive/input.js#L16
    private static readonly PURE_EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;;
    
    // Passwords should be at least 8 characters long and should contain one number, one character and one special character.
    // const PASSWORD_REGEXP = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
    //private static readonly PASSWORD_REGEXP = /^(?=.*[A-Za-z])[A-Za-z]{3,}$/;

    constructor() {
    }

    public static readonly regexValidators = {
        email: ValidatorProvider.PURE_EMAIL_REGEXP//,
        //password: ValidatorProvider.PASSWORD_REGEXP
    }
}
