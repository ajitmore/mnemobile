import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { AppRequestProvider } from "../app-request/app-request";
import { Config } from "../../utils/config";

/*
  Generated class for the LoginServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


export interface User {
    name: string;
    email: string;
    company: string;
}

@Injectable()
export class LoginServiceProvider {

    currentUser: User;

    constructor(private appRequest: AppRequestProvider) {
        console.log('Hello LoginServiceProvider Provider');
    }

    public login(credentials) {
        if (credentials.email === null || credentials.password === null) {
            return Observable.throw("Please insert credentials");
        } else {
            return Observable.create(observer => {
                // At this point make a request to your backend to make a real check!
                this.appRequest.postLoginRequest(Config.DB_SERVICE.Login, credentials)
                    .then(data => {
                        if (data) {
                            let access = true;//(credentials.password === "pass" && credentials.email === "email");
                            this.currentUser = { name: 'John Smith', company: 'DPI', email: credentials.email };

                            localStorage.setItem('CurrentUser', JSON.stringify(this.currentUser));

                            observer.next(access);
                            observer.complete();
                        }
                    })
            });
        }
    }

    public register(credentials) {
        if (credentials.email === null || credentials.password === null) {
            return Observable.throw("Please insert credentials");
        } else {
            // At this point store the credentials to your backend!
            return Observable.create(observer => {
                observer.next(true);
                observer.complete();
            });
        }
    }

    public getUserInfo(): User {
        return this.currentUser;
    }

    public logout() {
        return Observable.create(observer => {
            this.currentUser = null;
            observer.next(true);
            observer.complete();
        });
    }

}
