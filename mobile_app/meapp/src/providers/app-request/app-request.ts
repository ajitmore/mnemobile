import { HTTP } from "@ionic-native/http";
import { Injectable } from '@angular/core';

import { Config } from "../../utils/config";

/*
  Generated class for the AppRequestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AppRequestProvider {

    constructor(public http: HTTP) {
        //console.log('Hello AppRequestProvider Provider');
    }


    private getBaseURL(): string {
        return Config.DB_SERVICE.BASE_URL
    }

    private getHeader(token: string): Headers {
        let headerDict = {
            'Content-type': 'application/json',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type',
            'auth': 'TW9uaXRvckFuZEV2YWx1YXRl'
        }
        if (token) {
            headerDict["token"] = token;
        }
        const requestHeaders = new Headers(headerDict);
        return requestHeaders;
    }

    public async getRequest(service_url: string, data?: any, token?: string): Promise<any> {
        try {
            let promise = new Promise((success, reject) => {
                this.http.get(this.getBaseURL() + service_url, data,
                    { headers: this.getHeader(token) }
                )
                    .then(response => {
                        success(response);
                    })
                    .catch(err => {
                        reject(err);
                    })
            });
        } catch (error) {

        }
    }

    public async postRequest(service_url: string, data?: any, token?: string): Promise<any> {
        try {
            let promise = new Promise((success, reject) => {
                this.http.post(this.getBaseURL() + service_url, data,
                    {
                        headers: this.getHeader(token)
                    })
                    .then(response => {
                        success(response);
                    })
                    .catch(err => {
                        reject(err);
                    })
            })
        } catch (error) {

        }
    }

    public async postLoginRequest(service_url: string, credential: any): Promise<any> {
        try {
            let headers = this.getHeader(null);
            headers["username"] = credential.email;
            headers["password"] = credential.password;
            let promise = new Promise((success, reject) => {
                this.http.post(this.getBaseURL() + service_url, null, {
                    headers: headers
                })
                .then(data => {
                    success(data);
                })
                .catch(err => {
                    reject(err);
                });
            });
        } catch (error) {

        }
    }
}