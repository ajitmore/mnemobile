import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from "@angular/http";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { SeedsPage } from "../pages/seeds/seeds";

import { ValidatorProvider } from '../providers/validator/validator';
import { LoginServiceProvider } from '../providers/login-service/login-service';
import { AppRequestProvider } from '../providers/app-request/app-request';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SeedsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SeedsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ValidatorProvider,
    LoginServiceProvider,
    AppRequestProvider,
  ]
})
export class AppModule {}
